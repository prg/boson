class BosonGrammar:
    def __init__(self):
        self.__grammar_tree = None
        self.__error_index = None

    def get_grammar_tree(self):
        return self.__grammar_tree

    def set_grammar_tree(self, grammar_tree: tuple):
        self.__grammar_tree = grammar_tree

    grammar_tree = property(get_grammar_tree, set_grammar_tree)

    def get_error_index(self):
        return self.__error_index

    def set_error_index(self, error_index: int):
        self.__error_index = error_index

    error_index = property(get_error_index, set_error_index)


class BosonGrammarNode:
    def __init__(self):
        self.reduce_number = -1
        self.__data = []

    def __getitem__(self, item):
        return self.__data[item]

    def __iadd__(self, other):
        self.__data += other
        return self

    def append(self, item):
        self.__data.append(item)

    def insert(self, index, item):
        self.__data.insert(index, item)

    def data(self):
        return self.__data


class BosonGrammarAnalyzer:
    def __init__(self):
        self.__terminal_index = {
            'alphabet': 0,
            'count': 1,
            '$': 2,
            'plus': 3,
            'node': 4,
            'except': 5,
            'brace_l': 6,
            'dot': 7,
            'bracket_r': 8,
            'brace_r': 9,
            'comma': 10,
            'assign': 11,
            'string': 12,
            'parentheses_r': 13,
            'reduce': 14,
            'name': 15,
            'star': 16,
            'end': 17,
            'parentheses_l': 18,
            'greedy': 19,
            'or': 20,
            'bracket_l': 21,
            'command': 22,
            'null': 23
        }
        self.__action_table = {
            0: {15: 's1', 22: 's5'},
            1: {11: 's10', 14: 's9'},
            2: {2: 'r2', 15: 'r2', 22: 'r2'},
            3: {2: 'r98', 15: 'r98', 22: 'r98'},
            4: {2: 'r97', 15: 'r97', 22: 'r97'},
            5: {12: 's15', 15: 's14'},
            6: {2: 'r99', 15: 'r99', 22: 'r99'},
            7: {2: 'a'},
            8: {2: 'r71', 15: 's1', 22: 's5'},
            9: {11: 'r82', 12: 's25', 15: 's21', 17: 'r82', 18: 's26', 20: 'r82', 21: 's27', 23: 's24'},
            10: {0: 'r7', 5: 's33', 7: 'r7', 12: 'r7', 15: 'r7', 18: 's37', 21: 's34'},
            11: {12: 'r22', 15: 'r22', 17: 'r22'},
            12: {12: 's15', 15: 's14', 17: 's38'},
            13: {12: 'r39', 15: 'r39', 17: 'r39'},
            14: {12: 'r3', 15: 'r3', 17: 'r3'},
            15: {12: 'r4', 15: 'r4', 17: 'r4'},
            16: {2: 'r1', 15: 'r1', 22: 'r1'},
            17: {11: 's41', 17: 'r36', 20: 'r36'},
            18: {11: 'r80', 12: 's25', 15: 's21', 17: 'r80', 18: 's26', 20: 'r80', 21: 's27'},
            19: {17: 's44'},
            20: {17: 'r30', 20: 'r30'},
            21: {3: 's50', 8: 'r52', 11: 'r52', 12: 'r52', 13: 'r52', 15: 'r52', 16: 's49', 17: 'r52', 18: 'r52', 20: 'r52', 21: 'r52'},
            22: {11: 'r38', 12: 'r38', 15: 'r38', 17: 'r38', 18: 'r38', 20: 'r38', 21: 'r38'},
            23: {8: 'r84', 11: 'r84', 12: 'r84', 13: 'r84', 15: 'r84', 17: 'r84', 18: 'r84', 20: 'r84', 21: 'r84'},
            24: {11: 'r81', 17: 'r81', 20: 'r81'},
            25: {8: 'r86', 11: 'r86', 12: 'r86', 13: 'r86', 15: 'r86', 17: 'r86', 18: 'r86', 20: 'r86', 21: 'r86'},
            26: {12: 's25', 15: 's21', 18: 's26', 21: 's27'},
            27: {12: 's25', 15: 's21', 18: 's26', 21: 's27'},
            28: {0: 's58', 7: 's56', 12: 's59', 15: 's55'},
            29: {0: 'r7', 5: 's33', 6: 's64', 7: 'r7', 12: 'r7', 15: 'r7', 17: 'r70', 18: 's37', 20: 'r70', 21: 's34'},
            30: {0: 'r90', 5: 'r90', 6: 'r90', 7: 'r90', 8: 'r90', 12: 'r90', 13: 'r90', 15: 'r90', 17: 'r90', 18: 'r90', 20: 'r90', 21: 'r90'},
            31: {0: 'r65', 5: 'r65', 6: 'r65', 7: 'r65', 12: 'r65', 15: 'r65', 17: 'r65', 18: 'r65', 20: 'r65', 21: 'r65'},
            32: {17: 's65'},
            33: {0: 'r5', 7: 'r5', 12: 'r5', 15: 'r5'},
            34: {0: 'r7', 5: 's33', 7: 'r7', 12: 'r7', 15: 'r7', 18: 's37', 21: 's34'},
            35: {0: 'r6', 7: 'r6', 12: 'r6', 15: 'r6'},
            36: {17: 'r63', 20: 'r63'},
            37: {0: 'r7', 5: 's33', 7: 'r7', 12: 'r7', 15: 'r7', 18: 's37', 21: 's34'},
            38: {2: 'r74', 15: 'r74', 22: 'r74'},
            39: {12: 'r40', 15: 'r40', 17: 'r40'},
            40: {17: 'r79', 20: 'r79'},
            41: {15: 's73', 18: 'r33'},
            42: {17: 'r35', 20: 'r35'},
            43: {11: 'r37', 12: 'r37', 15: 'r37', 17: 'r37', 18: 'r37', 20: 'r37', 21: 'r37'},
            44: {2: 'r87', 15: 'r87', 22: 'r87'},
            45: {17: 'r83', 20: 's75'},
            46: {8: 'r85', 11: 'r85', 12: 'r85', 13: 'r85', 15: 'r85', 17: 'r85', 18: 'r85', 20: 'r85', 21: 'r85'},
            47: {8: 'r50', 11: 'r50', 12: 'r50', 13: 'r50', 15: 'r50', 17: 'r50', 18: 'r50', 20: 'r50', 21: 'r50'},
            48: {8: 'r51', 11: 'r51', 12: 'r51', 13: 'r51', 15: 'r51', 17: 'r51', 18: 'r51', 20: 'r51', 21: 'r51'},
            49: {0: 'r73', 5: 'r73', 6: 'r73', 7: 'r73', 8: 'r73', 11: 'r73', 12: 'r73', 13: 'r73', 15: 'r73', 17: 'r73', 18: 'r73', 19: 'r73', 20: 'r73', 21: 'r73'},
            50: {0: 'r72', 5: 'r72', 6: 'r72', 7: 'r72', 8: 'r72', 11: 'r72', 12: 'r72', 13: 'r72', 15: 'r72', 17: 'r72', 18: 'r72', 19: 'r72', 20: 'r72', 21: 'r72'},
            51: {13: 's76'},
            52: {8: 'r58', 12: 'r58', 13: 'r58', 15: 'r58', 18: 'r58', 20: 's79', 21: 'r58'},
            53: {8: 'r100', 12: 's25', 13: 'r100', 15: 's21', 18: 's26', 21: 's27'},
            54: {8: 's81'},
            55: {0: 'r10', 1: 'r10', 3: 'r10', 5: 'r10', 6: 'r10', 7: 'r10', 8: 'r10', 12: 'r10', 13: 'r10', 15: 'r10', 16: 'r10', 17: 'r10', 18: 'r10', 20: 'r10', 21: 'r10'},
            56: {0: 'r9', 1: 'r9', 3: 'r9', 5: 'r9', 6: 'r9', 7: 'r9', 8: 'r9', 12: 'r9', 13: 'r9', 15: 'r9', 16: 'r9', 17: 'r9', 18: 'r9', 20: 'r9', 21: 'r9'},
            57: {0: 'r12', 1: 'r12', 3: 'r12', 5: 'r12', 6: 'r12', 7: 'r12', 8: 'r12', 12: 'r12', 13: 'r12', 15: 'r12', 16: 'r12', 17: 'r12', 18: 'r12', 20: 'r12', 21: 'r12'},
            58: {0: 'r8', 1: 'r8', 3: 'r8', 5: 'r8', 6: 'r8', 7: 'r8', 8: 'r8', 12: 'r8', 13: 'r8', 15: 'r8', 16: 'r8', 17: 'r8', 18: 'r8', 20: 'r8', 21: 'r8'},
            59: {0: 'r11', 1: 'r11', 3: 'r11', 5: 'r11', 6: 'r11', 7: 'r11', 8: 'r11', 12: 'r11', 13: 'r11', 15: 'r11', 16: 'r11', 17: 'r11', 18: 'r11', 20: 'r11', 21: 'r11'},
            60: {0: 'r15', 1: 's82', 3: 's50', 5: 'r15', 6: 'r15', 7: 'r15', 8: 'r15', 12: 'r15', 13: 'r15', 15: 'r15', 16: 's49', 17: 'r15', 18: 'r15', 20: 'r15', 21: 'r15'},
            61: {0: 'r64', 5: 'r64', 6: 'r64', 7: 'r64', 12: 'r64', 15: 'r64', 17: 'r64', 18: 'r64', 20: 'r64', 21: 'r64'},
            62: {17: 'r91', 20: 'r91'},
            63: {17: 'r69', 20: 'r69'},
            64: {15: 's88'},
            65: {2: 'r93', 15: 'r93', 22: 'r93'},
            66: {0: 'r7', 5: 's33', 7: 'r7', 8: 'r102', 12: 'r7', 13: 'r102', 15: 'r7', 18: 's37', 21: 's34'},
            67: {0: 'r20', 5: 'r20', 7: 'r20', 8: 'r20', 12: 'r20', 13: 'r20', 15: 'r20', 18: 'r20', 20: 's90', 21: 'r20'},
            68: {8: 's93'},
            69: {17: 'r92', 20: 's95'},
            70: {13: 's96'},
            71: {18: 's98'},
            72: {18: 'r32'},
            73: {18: 'r31'},
            74: {17: 'r29', 20: 'r29'},
            75: {11: 'r82', 12: 's25', 15: 's21', 17: 'r82', 18: 's26', 20: 'r82', 21: 's27', 23: 's24'},
            76: {3: 's50', 8: 'r55', 11: 'r55', 12: 'r55', 13: 'r55', 15: 'r55', 16: 's49', 17: 'r55', 18: 'r55', 20: 'r55', 21: 'r55'},
            77: {8: 'r101', 13: 'r101', 20: 's79'},
            78: {8: 'r60', 13: 'r60', 20: 'r60'},
            79: {12: 's25', 15: 's21', 18: 's26', 21: 's27'},
            80: {8: 'r57', 12: 'r57', 13: 'r57', 15: 'r57', 18: 'r57', 21: 'r57'},
            81: {8: 'r75', 11: 'r75', 12: 'r75', 13: 'r75', 15: 'r75', 17: 'r75', 18: 'r75', 20: 'r75', 21: 'r75'},
            82: {0: 'r96', 5: 'r96', 6: 'r96', 7: 'r96', 8: 'r96', 12: 'r96', 13: 'r96', 15: 'r96', 17: 'r96', 18: 'r96', 20: 'r96', 21: 'r96'},
            83: {0: 'r14', 5: 'r14', 6: 'r14', 7: 'r14', 8: 'r14', 12: 'r14', 13: 'r14', 15: 'r14', 17: 'r14', 18: 'r14', 20: 'r14', 21: 'r14'},
            84: {0: 'r27', 5: 'r27', 6: 'r27', 7: 'r27', 8: 'r27', 12: 'r27', 13: 'r27', 15: 'r27', 17: 'r27', 18: 'r27', 19: 's106', 20: 'r27', 21: 'r27'},
            85: {0: 'r13', 5: 'r13', 6: 'r13', 7: 'r13', 8: 'r13', 12: 'r13', 13: 'r13', 15: 'r13', 17: 'r13', 18: 'r13', 20: 'r13', 21: 'r13'},
            86: {0: 'r89', 5: 'r89', 6: 'r89', 7: 'r89', 8: 'r89', 12: 'r89', 13: 'r89', 15: 'r89', 17: 'r89', 18: 'r89', 20: 'r89', 21: 'r89'},
            87: {9: 's109', 15: 's108'},
            88: {9: 'r67', 15: 'r67'},
            89: {0: 'r19', 5: 'r19', 7: 'r19', 8: 'r19', 12: 'r19', 13: 'r19', 15: 'r19', 18: 'r19', 21: 'r19'},
            90: {0: 'r7', 5: 's33', 7: 'r7', 12: 'r7', 15: 'r7', 18: 's37', 21: 's34'},
            91: {8: 'r23', 13: 'r23', 20: 'r23'},
            92: {8: 'r103', 13: 'r103', 20: 's90'},
            93: {0: 'r77', 5: 'r77', 6: 'r77', 7: 'r77', 8: 'r77', 12: 'r77', 13: 'r77', 15: 'r77', 17: 'r77', 18: 'r77', 20: 'r77', 21: 'r77'},
            94: {17: 'r62', 20: 'r62'},
            95: {0: 'r7', 5: 's33', 7: 'r7', 12: 'r7', 15: 'r7', 18: 's37', 21: 's34'},
            96: {0: 'r18', 1: 's82', 3: 's50', 5: 'r18', 6: 'r18', 7: 'r18', 8: 'r18', 12: 'r18', 13: 'r18', 15: 'r18', 16: 's49', 17: 'r18', 18: 'r18', 20: 'r18', 21: 'r18'},
            97: {17: 'r34', 20: 'r34'},
            98: {4: 'r46', 16: 's117'},
            99: {17: 'r28', 20: 'r28'},
            100: {8: 'r76', 11: 'r76', 12: 'r76', 13: 'r76', 15: 'r76', 17: 'r76', 18: 'r76', 20: 'r76', 21: 'r76'},
            101: {8: 'r53', 11: 'r53', 12: 'r53', 13: 'r53', 15: 'r53', 17: 'r53', 18: 'r53', 20: 'r53', 21: 'r53'},
            102: {8: 'r54', 11: 'r54', 12: 'r54', 13: 'r54', 15: 'r54', 17: 'r54', 18: 'r54', 20: 'r54', 21: 'r54'},
            103: {8: 'r61', 13: 'r61', 20: 'r61'},
            104: {8: 'r59', 13: 'r59', 20: 'r59'},
            105: {0: 'r95', 5: 'r95', 6: 'r95', 7: 'r95', 8: 'r95', 12: 'r95', 13: 'r95', 15: 'r95', 17: 'r95', 18: 'r95', 20: 'r95', 21: 'r95'},
            106: {0: 'r25', 5: 'r25', 6: 'r25', 7: 'r25', 8: 'r25', 12: 'r25', 13: 'r25', 15: 'r25', 17: 'r25', 18: 'r25', 20: 'r25', 21: 'r25'},
            107: {0: 'r26', 5: 'r26', 6: 'r26', 7: 'r26', 8: 'r26', 12: 'r26', 13: 'r26', 15: 'r26', 17: 'r26', 18: 'r26', 20: 'r26', 21: 'r26'},
            108: {9: 'r66', 15: 'r66'},
            109: {17: 'r68', 20: 'r68'},
            110: {8: 'r21', 13: 'r21', 20: 'r21'},
            111: {8: 'r24', 13: 'r24', 20: 'r24'},
            112: {17: 'r56', 20: 'r56'},
            113: {0: 'r17', 5: 'r17', 6: 'r17', 7: 'r17', 8: 'r17', 12: 'r17', 13: 'r17', 15: 'r17', 17: 'r17', 18: 'r17', 20: 'r17', 21: 'r17'},
            114: {0: 'r16', 5: 'r16', 6: 'r16', 7: 'r16', 8: 'r16', 12: 'r16', 13: 'r16', 15: 'r16', 17: 'r16', 18: 'r16', 20: 'r16', 21: 'r16'},
            115: {0: 'r78', 5: 'r78', 6: 'r78', 7: 'r78', 8: 'r78', 12: 'r78', 13: 'r78', 15: 'r78', 17: 'r78', 18: 'r78', 20: 'r78', 21: 'r78'},
            116: {10: 'r43', 13: 'r43'},
            117: {4: 'r44'},
            118: {4: 'r45'},
            119: {4: 's121'},
            120: {10: 's122', 13: 's124'},
            121: {10: 'r49', 13: 'r49', 17: 'r49', 18: 's98', 20: 'r49'},
            122: {4: 'r46', 16: 's117'},
            123: {10: 'r42', 13: 'r42'},
            124: {10: 'r88', 13: 'r88', 17: 'r88', 20: 'r88'},
            125: {10: 'r94', 13: 'r94', 17: 'r94', 20: 'r94'},
            126: {10: 'r48', 13: 'r48', 17: 'r48', 20: 'r48'},
            127: {10: 'r47', 13: 'r47', 17: 'r47', 20: 'r47'},
            128: {10: 'r41', 13: 'r41'}
        }
        self.__goto_table = {
            0: {21: 4, 37: 7, 47: 2, 49: 8, 51: 6, 55: 3},
            5: {7: 13, 15: 12, 44: 11},
            8: {21: 4, 47: 16, 51: 6, 55: 3},
            9: {4: 19, 12: 17, 28: 23, 33: 22, 34: 20, 59: 18},
            10: {1: 28, 3: 35, 5: 30, 9: 31, 24: 36, 56: 29, 60: 32},
            12: {7: 39, 44: 11},
            17: {32: 40, 39: 42},
            18: {28: 23, 33: 43},
            20: {61: 45},
            21: {35: 48, 40: 47, 54: 46},
            26: {18: 53, 22: 51, 28: 23, 33: 52},
            27: {18: 53, 22: 54, 28: 23, 33: 52},
            28: {41: 60, 53: 57},
            29: {1: 28, 3: 35, 5: 30, 9: 61, 13: 63, 19: 62},
            34: {1: 28, 3: 35, 5: 30, 9: 67, 42: 68, 50: 66},
            36: {14: 69},
            37: {1: 28, 3: 35, 5: 30, 9: 67, 42: 70, 50: 66},
            41: {20: 72, 45: 71},
            45: {27: 74},
            52: {0: 77, 36: 78},
            53: {28: 23, 33: 80},
            60: {2: 83, 29: 86, 40: 84, 62: 85},
            64: {52: 87},
            66: {1: 28, 3: 35, 5: 30, 9: 89},
            67: {38: 92, 48: 91},
            69: {46: 94},
            71: {57: 97},
            75: {12: 17, 28: 23, 33: 22, 34: 99, 59: 18},
            76: {11: 100, 25: 102, 40: 101},
            77: {36: 103},
            79: {28: 23, 33: 104},
            84: {10: 107, 23: 105},
            90: {1: 28, 3: 35, 5: 30, 9: 110},
            92: {48: 111},
            95: {1: 28, 3: 35, 5: 30, 9: 31, 24: 112, 56: 29},
            96: {8: 115, 17: 113, 40: 84, 62: 114},
            98: {30: 119, 31: 118, 63: 116},
            116: {16: 120},
            120: {43: 123},
            121: {6: 125, 57: 127, 58: 126},
            122: {30: 119, 31: 118, 63: 128}
        }
        self.__node_table = {
            0: ('*0', '1'),
            39: ('*0', '1'),
            73: ('0', ('*1', ('?',))),
            92: ('0', '2'),
            61: ('*0', '1'),
            62: (),
            91: ('0', ('*1', ('1',))),
            63: ('*0', '1'),
            65: ('*0', '1'),
            69: (),
            90: ('0', ('*1', ('1',))),
            6: (),
            14: (),
            88: (('*0', ('?',)), ('*1', ('?',)), ('2', ('?',))),
            17: (),
            77: ('1', ('*3', ('?',))),
            76: ('1',),
            18: ('*0', '1'),
            23: ('*0', '1'),
            101: ('*0',),
            102: ('0', ('*1', ('1',))),
            26: (),
            94: ('0', ('*1', ('?',))),
            86: ('0', '2'),
            28: ('*0', '1'),
            29: (),
            82: ('0', ('*1', ('1',))),
            32: (),
            35: (),
            78: ('0', ('*1', (('*1', ('?',)), '2'))),
            36: ('*0', '1'),
            79: ('*0',),
            41: ('*0', '1'),
            42: (),
            87: ('1', ('*2', ('1',))),
            45: (),
            48: (),
            93: (('*0', ('?',)), '1', ('*2', ('?',))),
            51: (),
            84: ('0', ('*1', ('?',))),
            85: ('0',),
            54: (),
            75: ('1', ('*3', ('?',))),
            74: ('1',),
            56: ('*0', '1'),
            60: ('*0', '1'),
            99: ('*0',),
            100: ('0', ('*1', ('1',)))
        }
        self.__reduce_symbol_sum = [2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 2, 1, 2, 1, 1, 2, 1, 1, 0, 2, 2, 0, 1, 1, 0, 3, 1, 0, 2, 1, 1, 2, 2, 2, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 2, 2, 1, 2, 1, 2, 2, 0, 2, 1, 2, 1, 3, 1, 0, 1, 1, 1, 3, 3, 4, 3, 4, 2, 1, 1, 0, 2, 1, 2, 1, 4, 4, 3, 1, 2, 2, 4, 3, 2, 1, 1, 1, 1, 1, 2, 1, 2]
        self.__reduce_to_non_terminal_index = [49, 49, 44, 44, 3, 1, 1, 53, 53, 53, 53, 41, 2, 29, 29, 17, 8, 8, 50, 50, 48, 7, 38, 38, 10, 23, 23, 27, 61, 61, 20, 45, 45, 39, 32, 32, 59, 59, 15, 15, 43, 16, 16, 31, 30, 30, 58, 6, 6, 35, 54, 54, 25, 11, 11, 46, 18, 18, 36, 0, 0, 14, 14, 56, 56, 52, 52, 13, 19, 19, 37, 40, 40, 21, 28, 28, 5, 5, 34, 12, 12, 12, 4, 33, 33, 33, 55, 57, 9, 9, 24, 60, 51, 63, 62, 62, 47, 47, 47, 22, 22, 42, 42]

    def __generate_grammar_tuple(self, statement_index, symbol_package):
        grammar_node = BosonGrammarNode()
        if isinstance(statement_index, int):
            node_tuple = self.__node_table[statement_index]
        else:
            node_tuple = statement_index
            statement_index = -1
        for i in node_tuple:
            if isinstance(i, str):
                if i == '$':
                    grammar_node.append(statement_index)
                elif i == '?':
                    grammar_node += symbol_package
                else:
                    if symbol_package:
                        if i[0] == '*':
                            grammar_node += symbol_package[int(i[1:])]
                        else:
                            grammar_node.append(symbol_package[int(i)])
            else:
                if symbol_package:
                    if i[0][0] == '*':
                        for node in symbol_package[int(i[0][1:])]:
                            grammar_node += self.__generate_grammar_tuple(i[1], node)
                    else:
                        for node in symbol_package[int(i[0])]:
                            grammar_node.append(self.__generate_grammar_tuple(i[1], node))
        grammar_node.reduce_number = statement_index
        return grammar_node

    def grammar_analysis(self, token_list):
        grammar = BosonGrammar()
        analysis_stack = [0]
        symbol_stack = []
        token_index = 0
        while token_index < len(token_list):
            token = token_list[token_index]
            current_state = analysis_stack[-1]
            operation = self.__action_table.get(current_state, {}).get(self.__terminal_index[token.symbol], 'e')
            operation_flag = operation[0]
            if operation_flag == 'e':
                grammar.error_index = token_index
                return grammar
            elif operation_flag == 's':
                state_number = int(operation[1:])
                analysis_stack.append(state_number)
                token_index += 1
                symbol_stack.append(token.text)
            elif operation_flag == 'r':
                statement_index = int(operation[1:]) - 1
                reduce_sum = self.__reduce_symbol_sum[statement_index]
                for _ in range(reduce_sum):
                    analysis_stack.pop()
                current_state = analysis_stack[-1]
                current_non_terminal_index = self.__reduce_to_non_terminal_index[statement_index]
                goto_next_state = self.__goto_table.get(current_state, {}).get(current_non_terminal_index, -1)
                if goto_next_state == -1:
                    raise ValueError('Invalid goto action: state={}, non-terminal={}'.format(current_state, current_non_terminal_index))
                analysis_stack.append(goto_next_state)
                if statement_index in self.__node_table:
                    symbol_package = []
                    for _ in range(reduce_sum):
                        symbol_package.insert(0, symbol_stack.pop())
                    symbol_stack.append(self.__generate_grammar_tuple(statement_index, symbol_package))
                elif statement_index in [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 15, 16, 19, 20, 21, 22, 24, 25, 27, 30, 31, 33, 34, 37, 38, 40, 43, 44, 46, 47, 49, 50, 52, 53, 55, 57, 58, 59, 64, 66, 67, 68, 70, 71, 72, 80, 81, 83, 89, 95, 96, 97, 98]:
                    grammar_node = BosonGrammarNode()
                    for _ in range(reduce_sum):
                        grammar_node.insert(0, symbol_stack.pop())
                    grammar_node.reduce_number = statement_index
                    symbol_stack.append(grammar_node)
                else:
                    raise ValueError('Invalid reduce number: reduce={}'.format(statement_index))
            elif operation_flag == 'a':
                grammar.grammar_tree = symbol_stack[0]
                return grammar
            else:
                raise ValueError('Invalid action: action={}'.format(operation))
        raise RuntimeError('Analyzer unusual exit.')


class BosonSemanticsAnalyzer:
    def __init__(self):
        self.__reduce_number_to_grammar_name = {
            73: 'command',
            92: 'lexical_define',
            77: 'lexical_closure',
            76: 'lexical_optional',
            102: 'lexical_select',
            86: 'reduce',
            93: 'grammar_node',
            84: 'name_closure',
            85: 'literal',
            75: 'complex_closure',
            74: 'complex_optional',
            100: 'select'
        }
        self.__reduce_number_to_grammar_number = {
            70: 0,
            96: 1,
            98: 2,
            97: 3,
            91: 6,
            90: 7,
            89: 8,
            88: 9,
            101: 12,
            94: 14,
            95: 15,
            82: 17,
            78: 18,
            79: 19,
            80: 20,
            81: 21,
            87: 22,
            83: 24,
            99: 29,
            71: 31,
            72: 32
        }
        self.__naive_reduce_number = {96, 97, 2, 98, 3, 71, 72, 9, 10, 8, 7, 80, 81, 83, 85, 89, 95}
        self.__semantics_entity = {}

    @staticmethod
    def __default_semantics_entity(grammar_entity):
        return grammar_entity

    @staticmethod
    def __naive_semantics_entity(grammar_entity):
        if len(grammar_entity) == 0:
            return None
        elif len(grammar_entity) == 1:
            return grammar_entity[0]
        else:
            return grammar_entity

    def __semantics_analysis(self, grammar_tree: BosonGrammarNode):
        if grammar_tree.reduce_number in self.__reduce_number_to_grammar_name:
            grammar_name = self.__reduce_number_to_grammar_name[grammar_tree.reduce_number]
        elif grammar_tree.reduce_number in self.__reduce_number_to_grammar_number:
            grammar_name = '!grammar_{}'.format(self.__reduce_number_to_grammar_number[grammar_tree.reduce_number])
        else:
            grammar_name = '!grammar_{}'.format('hidden')
        grammar_entity = list(map(lambda g: self.__semantics_analysis(g) if isinstance(g, BosonGrammarNode) else g, grammar_tree.data()))
        if grammar_name in self.__semantics_entity:
            return self.__semantics_entity[grammar_name](grammar_entity)
        elif grammar_tree.reduce_number in self.__naive_reduce_number:
            return self.__naive_semantics_entity(grammar_entity)
        else:
            return self.__default_semantics_entity(grammar_entity)

    def semantics_analysis(self, grammar_tree: BosonGrammarNode):
        return self.__semantics_analysis(grammar_tree)

    def semantics_entity(self, sign):
        def decorator(f):
            if isinstance(sign, int):
                name = '!grammar_{}'.format(sign)
            elif isinstance(sign, str):
                name = sign
            else:
                raise ValueError('Invalid grammar sign: {}'.format(sign))
            self.__semantics_entity[name] = f
            return f
        return decorator
